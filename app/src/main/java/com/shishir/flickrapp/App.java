package com.shishir.flickrapp;

import com.orm.SugarApp;
import com.shishir.flickrapp.utils.Utils;

/**
 * Created by Shishir on 11/6/2017.
 */

public class App extends SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();

        Utils.frescoConfig(this);
    }
}
