package com.shishir.flickrapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.shishir.flickrapp.R;
import com.shishir.flickrapp.data.IntroData;

import java.util.ArrayList;

public class IntroActivity extends AppIntro {

    Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String root = Environment.getExternalStorageDirectory().toString();

        Log.e("ROOT",root);
        ctx = IntroActivity.this;

        ArrayList<IntroData> introData = new ArrayList<>();
        introData.add(new IntroData("Welcome","Find images using the 'Search'",R.drawable.screen_1,Color.parseColor("#1976D2") ));
        introData.add(new IntroData("TA DA!","Scroll through images",R.drawable.screen_2,Color.parseColor("#1976D2") ));
        introData.add(new IntroData("Filter","Filter the results based on titles",R.drawable.screen_3,Color.parseColor("#1976D2") ));
        introData.add(new IntroData("Share","Feel free to share any image (Works with Deep Link btw;))",R.drawable.screen_4,Color.parseColor("#1976D2") ));

        for(IntroData data : introData){
            addSlide(AppIntroFragment.newInstance(data.getTitle(), data.getDescripiton(), data.getImage(), data.getColor()));
        }


        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        setVibrate(true);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        gotToSplashScreen();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        gotToSplashScreen();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }

    void gotToSplashScreen() {

        startActivity(new Intent(ctx, MainActivity.class));
        finish();
    }
}
