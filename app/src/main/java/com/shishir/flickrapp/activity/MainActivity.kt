package com.shishir.flickrapp.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.shishir.flickrapp.R
import com.shishir.flickrapp.adapter.FlickrPhotoAdapter
import com.shishir.flickrapp.adapter.SpacesItemDecoration
import com.shishir.flickrapp.data.Photo
import com.shishir.flickrapp.loader.changeVisibility
import com.shishir.flickrapp.loader.filterPhotoList
import com.shishir.flickrapp.loader.getPhotoList
import com.shishir.flickrapp.loader.getPhotoListFromDatabase
import com.shishir.flickrapp.utils.PermissionUtil
import com.shishir.flickrapp.utils.Utils
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URLEncoder
import java.util.HashMap
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val context = this
    var recyclerView: RecyclerView? = null
    var flickrPhotoAdapter: FlickrPhotoAdapter? = null

    var tv_empty: TextView? = null

    companion object {
        val TYPE_TAG = "type"
        val SEARCH = 1
        val FILTER = 2
        var photoList = ArrayList<Photo>()
        var photoHashMap = HashMap<String, Photo>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recyclerView = findViewById(R.id.photo_list);
        tv_empty = findViewById(R.id.tv_empty);

        recyclerView?.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        recyclerView?.addItemDecoration(SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.spacing)))

        flickrPhotoAdapter = FlickrPhotoAdapter(photoList, context, Utils.isNetworkAvailable(context))
        recyclerView?.setAdapter(flickrPhotoAdapter)


        checkPermission()
        deepLink()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        var inflater = getMenuInflater()

        inflater.inflate(R.menu.menu_items, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        val intent = Intent(context, SearchAndFilterActivity::class.java)

        if (item != null) {
            when (item.getItemId()) {

                R.id.action_search -> {
                    intent.putExtra(TYPE_TAG, SEARCH)
                    startActivityForResult(intent, SEARCH)
                }
                R.id.action_filter -> {
                    intent.putExtra(TYPE_TAG, FILTER)
                    startActivityForResult(intent, FILTER)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            var result = data?.getStringExtra("result")

            var isNetwork = Utils.isNetworkAvailable(context)
            flickrPhotoAdapter?.isNetwork = isNetwork

            when (requestCode) {

                SEARCH -> {
                    result?.let {

                        if (isNetwork) {
                            getPhotoList(URLEncoder.encode(result, "UTF-8"), flickrPhotoAdapter, context, tv_empty, recyclerView)
                        } else {
                            getPhotoListFromDatabase(URLEncoder.encode(result, "UTF-8"), flickrPhotoAdapter, tv_empty, recyclerView)
                        }
                    }
                }
                FILTER -> {
                    result?.let {
                        filterPhotoList(URLEncoder.encode(result, "UTF-8"), flickrPhotoAdapter, tv_empty, recyclerView)
                    }

                }

            }
        } else {

            flickrPhotoAdapter?.updateData(photoList)
            flickrPhotoAdapter?.notifyDataSetChanged()
        }


    }

    private fun deepLink() {

        try {
            val intent = intent
            val action = intent.action
            val data = intent.data

            val data_string = data.toString().split("/")
            var photo = Photo()
            photo.farm = Integer.parseInt(data_string[3])
            photo.server = data_string[4]
            photo.photo_id = data_string[5]
            photo.secret = data_string[6]

            val justOne = java.util.ArrayList<String>()
            justOne.add(Utils.urlGen(photo))
            val builder = ImageViewer.Builder(context, justOne)
                    .setFormatter { Utils.urlGen(photo) }
            builder.show()

        } catch (e: Exception) {

        }

    }


    override fun onResume() {
        super.onResume()

        changeVisibility(photoList.size, tv_empty, recyclerView)
    }

    internal fun checkPermission() {

        PermissionUtil.checkPermission(this@MainActivity, context, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                object : PermissionUtil.PermissionAskListener {
                    override fun onNeedPermission() {
                        ActivityCompat.requestPermissions(
                                this@MainActivity,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1
                        )
                    }

                    override fun onPermissionPreviouslyDenied() {
                        Utils.showToast(context, "Need this permission to view images offline.")
                    }

                    override fun onPermissionDisabled() {
                        Utils.showToast(context, "Permission Disabled.")
                    }

                    override fun onPermissionGranted() {
                        Utils.showToast(context, "Thank you! Appreciate it.")
                    }
                })

    }
}



