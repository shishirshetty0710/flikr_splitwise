package com.shishir.flickrapp.activity

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import com.shishir.flickrapp.R
import kotlinx.android.synthetic.main.activity_search_and_filter.*
import android.app.Activity
import android.content.Intent
import android.view.WindowManager


class SearchAndFilterActivity : AppCompatActivity(), SearchView.OnQueryTextListener {


    var type = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_and_filter)
        setSupportActionBar(toolbar)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setHomeButtonEnabled(true)


        type = intent.getIntExtra(MainActivity.TYPE_TAG,0)


        when(type){

            MainActivity.SEARCH ->{}

            MainActivity.FILTER ->{


            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {


        var inflater = getMenuInflater()

        inflater.inflate(R.menu.menu_items_searchfilter, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu?.findItem(R.id.action_search)
        val searchView = searchMenuItem?.getActionView() as SearchView


        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.isSubmitButtonEnabled = false
        searchView.setOnQueryTextListener(this)
        searchView.setIconifiedByDefault(false)

        when(type){

            MainActivity.SEARCH ->{
                searchView.queryHint = "Search eg. wildlife, climate, etc"}
            MainActivity.FILTER ->{
                searchView.queryHint = "Filter eg. elephants, snow, etc"}
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null) {
            when (item.getItemId()) {
                android.R.id.home -> {
                    finish();
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        val returnIntent = Intent()
        returnIntent.putExtra("result", p0)
        returnIntent.putExtra(MainActivity.TYPE_TAG, type)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
        return false
    }

    override fun onQueryTextChange(p0: String?): Boolean {

        return false
    }


}

