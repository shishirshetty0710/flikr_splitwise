package com.shishir.flickrapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shishir.flickrapp.R;
import com.shishir.flickrapp.data.Photo;
import com.shishir.flickrapp.utils.GlideApp;
import com.shishir.flickrapp.utils.ImageOverlayView;
import com.shishir.flickrapp.utils.Utils;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Shishir on 11/4/2017.
 */

public class FlickrPhotoAdapter extends RecyclerView.Adapter<FlickrPhotoAdapter.ViewHolder> {


    private List<Photo> mValues;

    Context context;

    private Random mRandom = new Random();

    boolean isNetwork;

    public FlickrPhotoAdapter(List<Photo> mValues, Context context, boolean isNetwork) {
        this.mValues = mValues;
        this.context = context;
        this.isNetwork = isNetwork;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);

        holder.img_photo_grid.getLayoutParams().height = getRandomIntInRange(750, 350);

        final String url = isNetwork? holder.mItem.getUrl(): "file://"+holder.mItem.getFile_url();
        GlideApp
                .with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.loading_spinner)
                .into(holder.img_photo_grid);

        holder.mView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, holder.mItem.getTitle(), Toast.LENGTH_SHORT).show();
                ArrayList<Photo> justOne = new ArrayList<>();
                justOne.add(holder.mItem);
                ImageViewer.Builder builder = new ImageViewer.Builder(context, justOne)
                        .setFormatter(new ImageViewer.Formatter<Photo>() {
                            @Override
                            public String format(Photo customImage) {

                                return url;
                            }

                        });
                ImageOverlayView overlayView = new ImageOverlayView(context);
                builder.setOverlayView(overlayView);
                builder.setImageChangeListener(getImageChangeListener(holder.mItem, overlayView));
                builder.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView img_photo_grid;
        public Photo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            img_photo_grid = (ImageView) view.findViewById(R.id.img_photo_grid);
        }
    }

    private int getRandomIntInRange(int max, int min) {
        return mRandom.nextInt((max - min) + min) + min;
    }

    private ImageViewer.OnImageChangeListener getImageChangeListener(final Photo photoObj, final ImageOverlayView overlayView) {
        return new ImageViewer.OnImageChangeListener() {
            @Override
            public void onImageChange(int position) {
                //String url = photoObj.getUrl();
                overlayView.setShareText(Utils.deepLinkGen(photoObj));
            }
        };
    }

    public void updateData(ArrayList<Photo> list) {
        mValues = list;
    }

    public boolean isNetwork() {
        return isNetwork;
    }

    public void setNetwork(boolean network) {
        isNetwork = network;
    }
}
