package com.shishir.flickrapp.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shishir.flickrapp.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shishir on 11/4/2017.
 */

public class ApiClient {

    public static final String BASE_TAG_URL = "https://api.flickr.com/services/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_TAG_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(ApiClient.okClient())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
    }
}
