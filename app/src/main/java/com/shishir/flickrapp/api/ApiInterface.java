package com.shishir.flickrapp.api;

import com.shishir.flickrapp.data.MainData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Shishir on 11/4/2017.
 */

public interface ApiInterface {

    //?format=json&nojsoncallback=1&api_key="+ Constants.FLICKR_KEY+"&method=flickr.photos.search"
    @GET("rest")
    Call<ResponseBody> getMainData(

            @Query("format") String format,
            @Query("nojsoncallback") int nojsoncallback,
            @Query("api_key") String api_key,
            @Query("method") String method,
            @Query("tags") String tags
    );


}
