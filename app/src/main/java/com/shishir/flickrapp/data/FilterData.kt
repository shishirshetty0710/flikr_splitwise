package com.shishir.flickrapp.data

import android.util.Log
import com.shishir.flickrapp.activity.MainActivity.Companion.photoHashMap

/**
 * Created by Shishir on 11/5/2017.
 */
fun generateHashMapForFilter(originalList: ArrayList<Photo>): HashMap<String, Photo> {

    //val photoHashMap = HashMap<String, Photo>()
    for (photoObj in originalList) {

        for (titleBreak in photoObj.title.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {

            photoHashMap.put(titleBreak, photoObj)
        }

    }

    return photoHashMap

}