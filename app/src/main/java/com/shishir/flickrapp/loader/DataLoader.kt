package com.shishir.flickrapp.loader

import android.content.Context
import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.orm.SugarRecord
import com.orm.query.Condition
import com.orm.query.Select
import com.shishir.flickrapp.activity.MainActivity
import com.shishir.flickrapp.adapter.FlickrPhotoAdapter
import com.shishir.flickrapp.api.ApiClient
import com.shishir.flickrapp.api.ApiInterface
import com.shishir.flickrapp.data.MainData
import com.shishir.flickrapp.data.Photo
import com.shishir.flickrapp.data.generateHashMapForFilter
import com.shishir.flickrapp.loader.ImageDownloader.downloadImage
import com.shishir.flickrapp.utils.Constants
import com.shishir.flickrapp.utils.Utils
import com.shishir.flickrapp.utils.Utils.getCacheUrl
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * Created by Shishir on 11/5/2017.
 */
internal fun getPhotoList(tag: String, flickrPhotoAdapter: FlickrPhotoAdapter?, context: Context, tv_empty: TextView?, recyclerView: RecyclerView?) {


    Log.e("FLIKR", tag)
    try {

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        val call = apiService.getMainData("json", 1, Constants.FLICKR_KEY, "flickr.photos.search", tag)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                var mainDataString = response.body().string()
                mainDataString = mainDataString.replace("\"id\"", "\"photo_id\"", false)

                val gson = Gson()
                val mainData = gson.fromJson(mainDataString, MainData::class.java)

                MainActivity.photoList = mainData.photos.photo as ArrayList<Photo>


                for (objPhoto in MainActivity.photoList) {
                    val url = Utils.urlGen(objPhoto)
                    objPhoto.tag = tag
                    objPhoto.url = url

                    var tempPath = objPhoto.photo_id + "_" + objPhoto.secret + "_z.jpg"
                    objPhoto.file_url = getCacheUrl() + tempPath
                    downloadImage(objPhoto.url, tempPath, context)
                }

                AsyncClass().execute()
                //SugarRecord.saveInTx(MainActivity.photoList)
                //generateHashMapForFilter(MainActivity.photoList as ArrayList<Photo>)

                changeVisibility(MainActivity.photoList.size, tv_empty, recyclerView)
                flickrPhotoAdapter?.updateData(MainActivity.photoList)
                flickrPhotoAdapter?.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Log.e("FLIKR", "onFailure")
            }
        })


    } catch (e: Exception) {
        e.printStackTrace()
    }

}

internal fun getPhotoListFromDatabase(tag: String, flickrPhotoAdapter: FlickrPhotoAdapter?, tv_empty: TextView?, recyclerView: RecyclerView?) {

    try {


        var photolist = Select.from(Photo::class.java)
                .where(Condition.prop("tag").like(tag))
                .list()


        MainActivity.photoList = photolist as ArrayList<Photo>

        changeVisibility(MainActivity.photoList.size, tv_empty, recyclerView)

        generateHashMapForFilter(MainActivity.photoList as ArrayList<Photo>)
        flickrPhotoAdapter?.updateData(MainActivity.photoList)
        flickrPhotoAdapter?.notifyDataSetChanged()

    } catch (e: Exception) {
        e.printStackTrace()
    }

}


internal fun filterPhotoList(tag: String, flickrPhotoAdapter: FlickrPhotoAdapter?, tv_empty: TextView?, recyclerView: RecyclerView?) {

    try {

        var photoList = ArrayList<Photo>()

        for (entry in MainActivity.photoHashMap) {
            if ((entry.key.toLowerCase()).contains(tag.toLowerCase())) {
                photoList.add(entry.value)
            }
        }

        flickrPhotoAdapter?.updateData(photoList)
        flickrPhotoAdapter?.notifyDataSetChanged()

        changeVisibility(photoList.size, tv_empty, recyclerView)

    } catch (e: Exception) {
        e.printStackTrace()
    }

}

internal fun changeVisibility(count: Int, tv_empty: TextView?, recyclerView: RecyclerView?) {

    if (count > 0) {
        recyclerView?.visibility = View.VISIBLE
        tv_empty?.visibility = View.INVISIBLE
    } else {
        recyclerView?.visibility = View.INVISIBLE
        tv_empty?.visibility = View.VISIBLE
    }
}

class AsyncClass :AsyncTask<Void, Void, Void>(){
    override fun doInBackground(vararg p0: Void?): Void? {

        SugarRecord.saveInTx(MainActivity.photoList)
        generateHashMapForFilter(MainActivity.photoList as ArrayList<Photo>)

     return null
    }


}