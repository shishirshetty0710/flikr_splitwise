package com.shishir.flickrapp.loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shishir on 11/5/2017.
 */

public class ImageDownloader {

    final static Set<Target> protectedFromGarbageCollectorTargets = new HashSet<>();

    public static void downloadImage(String imgaeUrl, final String fileUrl, Context context){

        Target bitmapTarget = new BitmapTarget(fileUrl);
        protectedFromGarbageCollectorTargets.add(bitmapTarget);

        Picasso.with(context)
                .load(imgaeUrl)
                .into(bitmapTarget);

    }


}

class BitmapTarget implements Target{

    String fileUrl;

    public BitmapTarget(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

        Log.e("myDir","onBitmapLoaded");
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/flickrcache");

            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            myDir = new File(myDir, fileUrl);
            Log.e("myDir",myDir.toString());
            FileOutputStream out = new FileOutputStream(myDir);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

            out.flush();
            out.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        Log.e("myDir","onBitmapFailed");
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        Log.e("myDir","onPrepareLoad");
    }
}
