package com.shishir.flickrapp.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Shishir on 11/4/2017.
 */

@GlideModule
public class MyAppGlideModule extends AppGlideModule {
}
