package com.shishir.flickrapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.shishir.flickrapp.data.Photo;

/**
 * Created by Shishir on 11/5/2017.
 */

public class Utils {

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void frescoConfig(Context context) {

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(context)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(context, config);

    }

    public static String urlGen(Photo objPhoto) {

        return "https://farm" + objPhoto.getFarm() + ".staticflickr.com/" + objPhoto.getServer() + "/" + objPhoto.getPhoto_id() + "_" + objPhoto.getSecret() + "_z.jpg";
    }

    public static String deepLinkGen(Photo objPhoto) {

        return "http://shishir.flickr.com/" + objPhoto.getFarm() + "/" + objPhoto.getServer() + "/" + objPhoto.getPhoto_id() + "/" + objPhoto.getSecret() + "";
    }

    public static String getCacheUrl() {

        return Environment.getExternalStorageDirectory().toString() + "/flickrcache/";
    }

    public static void showToast(Context ctx, String msg) {

        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }
}
